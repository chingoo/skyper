#!/bin/bash

APP_NAME="Skyper"
APP_PATH="/Users/$(whoami)/Applications/$APP_NAME.app"
SKYPE_PATH="/Applications/Skype.app"


function main() {

    if [ "$_system_name" != "OSX" ]; then
        echo "Mac OSX only"
        exit 1
    fi

    if [ ! -d "$SKYPE_PATH" ]; then
        SKYPE_PATH="/Users/$(whoami)$SKYPE_PATH"
        if [ ! -d "$SKYPE_PATH" ]; then
            echo "Skype not found"
            exit 1
        fi
    fi

    if [ -d "$APP_PATH" ]; then
        echo "$APP_NAME.app already exists"
        exit 1
    fi

    osacompile -x -o "$APP_PATH" -e "do shell script \"/bin/sh -c \\\"open -na $SKYPE_PATH --args -DataPath /Users/$(whoami)/Library/Application\\\ Support/$APP_NAME\\\"\""

}


function icon() {

    read -p "Change icon to skype black? (y/N) " -n 1 -r

    case "$REPLY" in
        y|Y)
            echo -e "\nDownloading"
            icon_get
            ;;
        n|N)
            echo -e "\nCya"
            exit 0
            ;;
        *)
            echo "Cya"
            exit 0
            ;;
    esac

    echo "More colors here: http://www.iconsdb.com/black-icons/skype-icon.html"

}


function icon_get() {

    curl --fail -o applet.icns http://www.iconsdb.com/icons/download/black/skype-512.icns

    if [ $? -ne 0 ]; then
        echo "Something goes wrong. Change icon manually"
        exit 1
    else
        mv $PWD/applet.icns $APP_PATH/Contents/Resources/applet.icns
        touch $APP_PATH
    fi

}

main
icon
