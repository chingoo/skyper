## Skyper

Run two skype accounts on Mac OSX simultaneously.

### Install

    bash <(curl -sL https://bitbucket.org/chingoo/skyper/downloads/Skyper.sh)
    
### Usage

Skyper.app will be created in `/Users/<username>/Applications/`.
User settings will be in `/Users/<username>/Library/Application\ Support/Skyper`.

### Uninstall

    rm -rf /Users/$(whoami)/Applications/Skyper.app
    rm -rf /Users/$(whoami)/Library/Application\ Support/Skyper
